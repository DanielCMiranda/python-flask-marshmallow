#!/usr/bin/make -f
# -*- makefile -*-

PYTHONPATH := ${CURDIR}
export PYTHONPATH

export env $(cat .env)


all: help
help:
	@echo ""
	@echo "-- Help Menu"
	@echo ""
	@echo "   1. make clean 			- Clean all pyc and caches"
	@echo "   2. make run				- Run application locally"
	@echo "   3. make black 			- Run black"
	@echo "   4. make check_black 		- Run check for black"
	@echo "   5. make lint				- Run lint report"
	@echo ""
	@echo ""


.PHONY: clean
clean:
	@echo "Clean files pyc and caches..."
	rm -rf build/ dist/ docs/_build *.egg-info
	find $(CURDIR) -name "*.py[co]" -delete
	find $(CURDIR) -name "*.orig" -delete
	find $(CURDIR)/$(MODULE) -name "__pycache__" | xargs rm -rf
	find $(CURDIR)/$(MODULE) -name ".pytest_cache" | xargs rm -rf


.PHONY: run
run:
	pipenv run flask run

.PHONY: black
black:
	pipenv run black -l 79 -t py37 .

.PHONY: check_black
check_black:
	pipenv run black -l 79 -t py37 . --check

.PHONY: lint
lint:
	find . -iname "*.py" | grep -v __pycache__ | grep -v test | grep -v .history | xargs pipenv run pylint --load-plugins pylint_flask_sqlalchemy
	#find . -iname "*.py" | grep -v __pycache__ | grep -v test | grep -v .history | xargs pipenv run pylint --reports=n --msg-template "{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" > pylint-report.txt || exit 0;

