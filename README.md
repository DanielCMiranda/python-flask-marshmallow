# Installation guide

""" Requirements

* [Brew](https://brew.sh/)


""" Install required tools

Execute the following commands to install the required tools:

`brew install pyenv`

`pyenv install 3.7.4`

`pip install pipenv --user`


""" Install project dependencies

**On the project root folder**, execute the following command:

`pipenv install --dev --python ~/.pyenv/versions/3.7.4/bin/python`


""" Run the project locally

`make run`

You can confirm that the application is running properly checking the application's swagger url:

http://127.0.0.1:5000/api/v1/ui/


""" Make request to the API

```
curl -X POST -H "Content-Type: application/json" http://127.0.0.1:5000/api/v1/users/ -d '{"name": "Fulano", "email": "fulano@bla.com"}'
```

```
curl -X GET -H "Content-Type: application/json" http://127.0.0.1:5000/api/v1/users/1
```