"""Start point to run the application"""

from app import APP

if __name__ == "__main__":
    APP.run()
