"""User model"""

import datetime
from marshmallow import fields, Schema
from . import DB


class UserModel(DB.Model):
    """User Model"""

    __tablename__ = "users"

    id = DB.Column(DB.Integer, primary_key=True)
    name = DB.Column(DB.String(128), nullable=False)
    email = DB.Column(DB.String(128), unique=True, nullable=False)
    created_at = DB.Column(DB.DateTime)
    modified_at = DB.Column(DB.DateTime)

    # class constructor
    def __init__(self, data):
        """
        Class constructor
        """
        self.name = data.get("name")
        self.email = data.get("email")
        self.created_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

    def save(self):
        """Save a new user"""
        DB.session.add(self)
        DB.session.commit()

    def update(self, data):
        """Update a specific user"""
        for key, item in data.items():
            setattr(self, key, item)
        self.modified_at = datetime.datetime.utcnow()
        DB.session.commit()

    def delete(self):
        """Delete the user"""
        DB.session.delete(self)
        DB.session.commit()

    @staticmethod
    def get_all_users():
        """Fetch all users"""
        return UserModel.query.all()

    @staticmethod
    def get_one_user(user_id):
        """Get a user by id"""
        return UserModel.query.get(user_id)

    @staticmethod
    def get_user_by_email(email):
        """Get a user by e-mail"""
        return UserModel.query.filter(UserModel.email == email).first()

    def __repr(self):
        """Defines the presentation of an instance as string"""
        return "<id {}>".format(self.id)


class UserSchema(Schema):
    """User Schema"""

    id = fields.Int(dump_only=True)
    name = fields.Str(required=True)
    email = fields.Email(required=True)
    created_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)
