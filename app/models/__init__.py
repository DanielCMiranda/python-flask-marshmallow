"""Initializes resources related to SQL"""

from flask_sqlalchemy import SQLAlchemy

# initialize our db
DB = SQLAlchemy()
