"""Initializes the application"""

import os

from flask import Flask
from app.config import APP_CONFIG
from app.views.user_view import USER_API as user_blueprint
from app.models import DB


def create_app():
    """Create app"""

    env_name = os.getenv("FLASK_ENV", "development")

    # app initiliazation
    flask_app = Flask(__name__)

    flask_app.config.from_object(APP_CONFIG[env_name])

    flask_app.register_blueprint(user_blueprint, url_prefix="/api/v1/users")

    return flask_app


def init_database(flask_app):
    """Initializes database"""

    env_name = os.getenv("FLASK_ENV", "development")

    DB.init_app(flask_app)
    # Create all the tables
    if env_name == "development":
        with flask_app.app_context():
            DB.create_all()
            DB.session.commit()


APP = create_app()
init_database(APP)
