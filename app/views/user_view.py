"""User view"""

from flask import request, json, Response, Blueprint
from app.models.user_model import UserModel, UserSchema

USER_API = Blueprint("users", __name__)
USER_SCHEMA = UserSchema()


@USER_API.route("/", methods=["POST"])
def create():
    """
    Create a new user
    """
    req_data = request.get_json()
    data = USER_SCHEMA.load(req_data)

    # Check if user already exist in the db
    user_in_db = UserModel.get_user_by_email(data.get("email"))
    if user_in_db:
        message = {
            "error": "User already exist, please supply another email address"
        }
        return custom_response(message, 400)

    user = UserModel(data)
    user.save()

    ser_data = USER_SCHEMA.dump(user)

    return custom_response(ser_data, 201)


@USER_API.route("/<user_id>", methods=["GET"])
def get(user_id):
    """
    Find an user by its id
    """

    user = UserModel.get_one_user(user_id)

    ser_data = USER_SCHEMA.dump(user)

    return custom_response(ser_data, 200)


def custom_response(res, status_code):
    """
    Custom Response Function
    """
    return Response(
        mimetype="application/json",
        response=json.dumps(res),
        status=status_code,
    )
