"""Centralizes the configurations of the application"""
# pylint: disable=too-few-public-methods

import os


class Development:
    """
    Development environment configuration
    """

    DEBUG = True
    TESTING = False
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", "sqlite:///:memory:")
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class Production:
    """
    Production environment configurations
    """

    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL")


APP_CONFIG = {"development": Development, "production": Production}
